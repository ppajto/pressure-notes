# Pressure notes
This piece of text should serve as an introdutcory info, enabling reader to use
MD simulations to obtain pressure profiles.

## Software for pressure calculations
So far, I had opporutunity to use two codes for pressure calculations. Modified
version of Gromacs by _Vanegas et al._ -> Gromacs_LS and version of Gromacs modified
by _M.Sega and C. Allolio_ here referenced as Sega's code. Let's cover the main 
differences and connected issues.

## * Gromacs_LS
Available with documentation on site [MDStress](https://mdstress.org/index.php/gromacs-ls/).
I have compiled Gromacs\_LS within my directory in _projects/softmatter_. One important
thing to know, is that Gromacs_LS is based in Gromacs 4.x. As a result, few modifications
of input files are required.

### Modifications of _.mdp_ file
To calculate pressure tensor (in case of LS stress tensor) we need to store positions
and velocities of all the particles. To do so, compressed trajectory file is not
sufficient and we need to use good old _.trr_ files.

In all atom simulations with timestep of 2 fs, frequency of storing positions and velocities is:
```
nstxout = 2500
nstvout = 2500
```

1. PME
2. group scheme
3. comm-grps
4. output for pressure calculations

Need to fix and center trajectory --> `trjconv_LS -pbc whole -ur compact -center`

### Force decompositions

### Analysing the binary _localstress.dat0_
If you are unable to read binary files as other mere mortals, developers of LS 
created _tensortools_. It is a set of tools for analysing and processing of raw 
stress tensor data.

Since we are mainly focused on the pressure profiles with respect to the axis 
perpendicular to the studied membrane, the example is shown in **BASH** file [profile.sh]()


## * Sega's version of Gromacs

### Modifications of _.mdp_ file

### Usage of constraints

### Resulting modification of water topology