#!/bin/bash
#Script for calculation of pressure profile from binary stress tensor outputs

source /storage/projects/softmatter/ivosek/gromacs.4.5.5-ls/bin/GMXRC

#######################
TITLE="REF_slipids.csv"
LOW=1
HIGH=500
#######################

module add python:2.7.5

export PYTHONPATH=$PYTHONPATH:/storage/projects/softmatter/ivosek/gromacs.4.5.5-ls/bin/tensortools

#tensortools -f $(for i in $(eval echo "{$LOW..$HIGH}"); do printf "localstress${i}.dat0 " ;done) -o localstress.dat0

tensortools -f localstress*.dat0 -o localstress.dat0


tensortools -f localstress.dat0 --prof z --oformat txt -o out

echo "z,sigma_xx,sigma_yy,sigma_zz" > $TITLE
awk '{if (NF == 10) {print $1, $2, $6, $10} }' < out >> $TITLE

